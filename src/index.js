const server = require('./server');

async function bootstrap () {
    try {
        server.listen(3000, async () => {
            console.log('server listening on port 3000!')
        });
    } catch (error) {
        console.error(error);
    }
}

bootstrap();
