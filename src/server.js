const express = require('express');
const morgan = require('morgan');

const server = express();

server.use(morgan('tiny'));

server.get('/', async (req, res) => {
    res.send('Hello express app !')
});

server.use('*', async (req, res, next) => {
    res.status(404).send('404 Not found')
});

module.exports = server;
